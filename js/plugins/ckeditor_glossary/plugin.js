/**
 * @file
 * CKEditor Glossary plugin definition.
 */

 CKEDITOR.config.contentsCss = '/css/ckeditor_overrides.css' ;
 CKEDITOR.plugins.add('ckeditor_glossary', {
   init: function(editor) {
     'use strict';
     var config = editor.config

     editor.addCommand('ckeditor_glossary_template', {
       exec: function(editor) {
         var selectedHtml = "";
         var firstLetter = "";
         var clearedString = "";
         var selection = editor.getSelection();
         selectedHtml = getSelectionHtml(selection);
         if(selectedHtml){
             firstLetter = getFirstLetter(selectedHtml);
             clearedString = clearString(selectedHtml);
             if(config.link){
              editor.insertHtml('<a href="' + config.link + '/' + firstLetter + '#' + clearedString + '" class="glossary-entry">' + selectedHtml + '</a>');
            }else{
              editor.insertHtml('<a href="/glossary/' + firstLetter + '#' + clearedString + '" class="glossary-entry">' + selectedHtml + '</a>');
            }
         }
       }
     });

     editor.ui.addButton('ckeditor_glossary', {
       label: 'Glossary (WYSIWYG)',
       toolbar: 'insert',
       command: 'ckeditor_glossary_template',
       icon: this.path + 'images/icon.png'
     });
   }
 });

 /**
  Get HTML of a selection.
  */
 function getSelectionHtml(selection) {
   var ranges = selection.getRanges();
   var html = '';
   for (var i = 0; i < ranges.length; i++) {
     var content = ranges[i].extractContents();
     html += content.getHtml();
   }
   return html;
 }

  /**
  Get first letter of the selected word.
  */
  function getFirstLetter(word) {
    return word.charAt(0);
  }

  /**
  Clear the string of accents, special characters and convert to lower case.
  */
  function clearString(string) {
    return string.normalize("NFD").replace(/\p{Diacritic}/gu, "").replace(/\s+/g, '-').toLowerCase();
  }