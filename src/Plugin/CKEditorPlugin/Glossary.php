<?php

namespace Drupal\ckeditor_glossary\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Glossary" plugin, with a CKEditor.
 *
 * @CKEditorPlugin(
 *   id = "ckeditor_glossary",
 *   label = @Translation("Glossary")
 * )
 */
class Glossary extends CKEditorPluginBase implements CKEditorPluginConfigurableInterface {

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    // Provide the JS plugin path.
    return \Drupal::service('extension.list.module')->getPath('ckeditor_glossary') . '/js/plugins/ckeditor_glossary/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $iconImage = \Drupal::service('extension.list.module')->getPath('ckeditor_glossary') . '/js/plugins/ckeditor_glossary/images/icon.png';

    // Return the CKEditor plugin button details.
    return [
      'ckeditor_glossary' => [
        'label' => $this->t('Link to Glossary'),
        'image' => $iconImage,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    $settings = $editor->getSettings();
    $config = [];

    if (!empty($settings['plugins']['ckeditor_glossary']['link'])) {
      $config['link'] = $settings['plugins']['ckeditor_glossary']['link'];
    }

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {
    $settings = $editor->getSettings();

    $form['link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path to glossary page'),
      '#description' => $this->t('Enter the path to the glossary page. Example: <em>/my-glossary-page</em>. Leave blank to use the default page <em>/glossary</em> that Drupal core ships with.'),
      '#default_value' => !empty($settings['plugins']['ckeditor_glossary']['link']) ? $settings['plugins']['ckeditor_glossary']['link'] : '',
    ];

    return $form;
  }

}
