## CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Maintainers

## INTRODUCTION

  The CKEditor Glossary (ckeditor_glossary) module provides a CKEditor
  button to allow users to create links in their text editors that point
  to a glossary page. The module has a configuration to change the path of
  the glossary and by default uses the Drupal glossary view. The links
  constructed have the following format:
  yourdomain/your-configured-link/first-letter-of-the-word#word-in-lowercase-and-without-special-characters.
  This provides an URL fragment that can be used to a scroll functionality
  in the glossary page.

## REQUIREMENTS

  * This module requires no modules outside of Drupal core.

## INSTALLATION

  * Install as you would normally install a contributed Drupal module.
    Visit
    <https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules>
    for further information.

## CONFIGURATION

  * In your text editors a new option will appear. Simply configure the
    path to your form or leave blank to use /glossary.

## MAINTAINERS

  * Current maintainers: [nsalves](https://www.drupal.org/u/nsalves)

This project has been sponsored by:
  * NTT DATA

NTT DATA – a part of NTT Group – is a trusted global innovator of IT and business services headquartered in Tokyo. NTT is one of the largest IT services provider in the world and has 140,000 professionals, operating in more than 50 countries. NTT DATA supports clients in their digital development through a wide range of consulting and strategic advisory services, cutting-edge technologies, applications, infrastructure, modernization of IT and BPOs. We contribute with vast experience in all sectors of economic activity and have extensive knowledge of the locations in which we operate