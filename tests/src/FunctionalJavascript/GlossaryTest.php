<?php

namespace Drupal\Tests\ckeditor_glossary\FunctionalJavascript;

use Drupal\Component\Serialization\Json;
use Drupal\field\Entity\FieldConfig;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\node\Entity\NodeType;

/**
 * CKEditor Glossary tests.
 *
 * @group ckeditor_glossary
 */
class GlossaryTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  static protected $modules = [
    'ckeditor_glossary',
    'ckeditor_glossary_test',
    'node',
    'filter',
    'field_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    NodeType::create([
      'type' => 'article',
      'name' => 'Article',
    ])->save();

    FieldConfig::create([
      'field_name' => 'body',
      'entity_type' => 'node',
      'bundle' => 'article',
      'format' => 'full_html',
    ])->save();

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');
    $display_repository->getViewDisplay('node', 'article')
      ->setComponent('body')
      ->save();
    $display_repository->getFormDisplay('node', 'article')
      ->setComponent('body')
      ->save();

    $this->drupalCreateNode([
      'type' => 'article',
      'title' => 'Article Glossary',
      'nid' => 1,
      'body' => [
        [
          'value' => $this->randomMachineName(32),
          'format' => 'full_html',
        ],
      ],
    ])->save();

    $permissions = [
      'bypass node access',
      'administer site configuration',
      'use text format full_html',
      'administer filters',
    ];
    $user = $this->createUser($permissions);
    $this->drupalLogin($user);
  }

  /**
   * Tests Glossary.
   */
  public function testGlossary() {
    // Verify that article are displayed.
    $this->drupalGet('/node/1');
    $this->assertSession()->pageTextContains('Article Glossary');

    // Asserts ckeditor button is present in the toolbar.
    $this->drupalGet('/node/1/edit');
    $this->assertSession()->elementExists('css', '#cke_edit-body-0-value .cke_button__ckeditor_glossary');

  }

}
